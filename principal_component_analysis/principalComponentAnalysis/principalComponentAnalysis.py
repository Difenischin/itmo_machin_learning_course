import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from sklearn.datasets import load_breast_cancer
from sklearn.decomposition import PCA
from sklearn.cross_validation import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import cross_val_score      

# detailed explanation of PCA is here
# https://plot.ly/ipython-notebooks/principal-component-analysis/

def main():
    constructScatteringDiagram(getDatasetsBreast())
    constructPCADiagram(getDatasetsBreast())
    viewCovarianceMatrix()
    checkOrthogonality()
    checkEigenVectorAndvariance()
    checkTraceMatrix()
    viewGraficsDispersion()
    
    learnKNeighborsClassifierByDatasetsBreast()
    learnKNeighborsClassifierByPCADatasetsBreast()
    


def constructScatteringDiagram(datasetsBreast):

    def getFrameGraphics(points , reaction):

        def getBaseFrameGraphics():
            baseFrameGraphics = plt
            baseFrameGraphics.figure(2, figsize=(8, 6))
            baseFrameGraphics.clf()# Побочный эффект
            baseFrameGraphics.xlabel(datasetsBreast.feature_names[0])
            baseFrameGraphics.ylabel(datasetsBreast.feature_names[1])
            return baseFrameGraphics

        def plotTrainingPoints(frameGraphics, points, reaction):
            frameGraphics.scatter(points[:, 0], points[:, 1], c=reaction, cmap=frameGraphics.cm.Set1,
                edgecolor='k')
            return frameGraphics

        def setLimitsCurrentAxes(frameGraphics, points):
            def getXMinPoints(points):
                 return points[:, 0].min() - .5
            def getXMaxPoints(points):
                 return points[:, 0].max() + .5
            def getYMinPoints(points):
                 return points[:, 1].min() - .5
            def getYMaxPoints(points):
                 return points[:, 1].max() + .5

            frameGraphics.xlim(getXMinPoints(points), getXMaxPoints(points))
            frameGraphics.xticks(())

            frameGraphics.ylim(getYMinPoints(points), getYMaxPoints(points))
            frameGraphics.yticks(())

            return frameGraphics

        frameGraphics = getBaseFrameGraphics()
        frameGraphics = plotTrainingPoints(frameGraphics, points, reaction)
        frameGraphics = setLimitsCurrentAxes(frameGraphics, points)

        return frameGraphics


    points = getNormalize(datasetsBreast.data[:, :2])
    reaction = getNormalize(datasetsBreast.target)
    #print(dir(datasetsBreast))
    #print(datasetsBreast.feature_names)
    frameGraphics = getFrameGraphics(points , reaction)
    frameGraphics.show()

def getDatasetsBreast():
     return load_breast_cancer()

def getNormalize(data):
    return StandardScaler().fit_transform(data)

def getReducedPCADatasets(points):
    pca = PCA(n_components=3)
    reducedDataset = pca.fit_transform(points)
    return reducedDataset

def constructPCADiagram(datasetsBreast):

    def createPCADiagram(points, reaction):

        def getBaseFigure3D():
            baseFigure = plt.figure(1, figsize=(8, 6))
            ax = Axes3D(baseFigure, elev=-150, azim=110)
            return ax

        def plotTrainingPoints(figure3D, reducedPoints, reaction):
            figure3D.scatter(reducedPoints[:, 0], reducedPoints[:, 1], reducedPoints[:, 2], c=reaction, cmap=plt.cm.Set1, edgecolor='k', s=40)
            return figure3D

        def createLabels(figure3D):
            figure3D.set_title("First three PCA directions")
            figure3D.set_xlabel("1st eigenvector")
            figure3D.w_xaxis.set_ticklabels([])
            figure3D.set_ylabel("2nd eigenvector")
            figure3D.w_yaxis.set_ticklabels([])
            figure3D.set_zlabel("3rd eigenvector")
            figure3D.w_zaxis.set_ticklabels([])
            return figure3D


        reducedPoints = getReducedPCADatasets(points)
        print('Our reduced X: \n', points)
        print('Sklearn reduced X: \n', reducedPoints)
        figure3D = getBaseFigure3D()
        figure3D = plotTrainingPoints(figure3D, reducedPoints, reaction)
        figure3D = createLabels(figure3D)

    points = getNormalize(datasetsBreast.data)
    reaction = getNormalize(datasetsBreast.target)

    createPCADiagram(points , reaction)
    plt.show()


def getCovarianceMatrixReducedPCADatasets():
    reducedPCADatasets = getReducedPCADatasets(getNormalize(getDatasetsBreast().data))
    ovarianceMatrixReducedPCADatasets = getCovarianceMatrix(reducedPCADatasets)
    return ovarianceMatrixReducedPCADatasets

def getCovarianceMatrixDatasets():
    return getCovarianceMatrix(getNormalize(getDatasetsBreast().data))

def getCovarianceMatrix(matrix):
    return np.cov(matrix.transpose())

def viewCovarianceMatrix():
    print('Матрица ковариации исходного набора данных X: \n', getCovarianceMatrixDatasets())
    print('Размерность матрицы ковориации для исходного набора данных X: \n', getCovarianceMatrixDatasets().shape)

    print('Матрица ковариации для исходного набора, спроецированного на главные компоненты  X: \n', getCovarianceMatrixReducedPCADatasets())
    print('Размерность матрицы ковориации для исходного набора данных, спроецированного на главные компоненты X: \n', getCovarianceMatrixReducedPCADatasets().shape)

def checkOrthogonality():

    ovarianceMatrixReducedPCA = getReducedPCADatasets(getNormalize(getDatasetsBreast().data))
    print('Исходного набора данных, спроецированного на главные компоненты  X: \n', ovarianceMatrixReducedPCA)
    print('Размерность исходного набора данных X: \n', ovarianceMatrixReducedPCA.shape)
    a = ovarianceMatrixReducedPCA[:, 0]
    print('Проверка на ортогональность вектора ab \n', a.shape)
    print('Проверка на ортогональность вектора a \n', a)
    b = ovarianceMatrixReducedPCA[:, 1]
    c = ovarianceMatrixReducedPCA[:, 2]

    ab = a.dot(b)
    ac = a.dot(c)
    bc = b.dot(c)

    abc = ab + ac + bc
    print('Произведение векторов abc \n', abc)


def getEigenVector(matrix):
    eig, vectors = np.linalg.eig(matrix)
    return vectors
    
def getEigenValue(matrix):
    return np.linalg.eigvals(matrix)

def checkEigenVectorAndvariance():
    
    def getDiagonalVector(matrix):
        return matrix.diagonal()
    
    def getDispersion(matrix):
        return np.var(matrix,axis=0)
    
    #Сравнить собственные значения матрицы ковариации X со значениями дисперсии главных компонент.
    eigenValueDatasets = getEigenValue(getCovarianceMatrixDatasets())
    print('Собственные значения матрицы ковариации', eigenValueDatasets)
    
    eigenValueReducedPCADatasets = getEigenValue(getCovarianceMatrixReducedPCADatasets())
    print('Собственные значения матрицы ковариации на спроецированном наборе', eigenValueReducedPCADatasets)

    dispersion = getDispersion(getReducedPCADatasets(getNormalize(getDatasetsBreast().data)))
    print('Дисперсия матрицы ковариации на спроецированном наборе', dispersion)


def checkTraceMatrix():
    def getTraceMatrix(matrix):
        return matrix.diagonal().sum()
    
    traceCovarianceMatrixDatasets = getTraceMatrix(getCovarianceMatrixDatasets())
    print('След матрицы ковариации на исходном наборе', traceCovarianceMatrixDatasets)
    traceCovarianceMatrixReducedPCADatasets = getTraceMatrix(getCovarianceMatrixReducedPCADatasets())
    print('След матрицы ковариации на спроецированном наборе', traceCovarianceMatrixReducedPCADatasets)

def viewGraficsDispersion():
    def viewGraficsDispersionStart():
        plt.xlabel('х')
        plt.ylabel('y')
        plt.plot(getExplainedDispersionPCADatasets())
        plt.plot(getExplainedDispersionDatasets())
        plt.legend(('доля объясненной дисперсии PCA','доля объясненной дисперси исходного набора данных'), loc=(0.8,0.4))
        plt.show()
     
    def getExplainedDispersionPCADatasets():
        covarianceMatrixPCA = getCovarianceMatrixReducedPCADatasets()
        eigenValueReducedPCADatasets = getEigenValue(covarianceMatrixPCA)
        return getExplainedDispersion(eigenValueReducedPCADatasets)
    
    def getExplainedDispersion(eigenValueDatasets):
        traceMatrix = sum(eigenValueDatasets)
        explainedDispersion = [(i / traceMatrix) for i in sorted(eigenValueDatasets, reverse=True)]
        return explainedDispersion
    
    def getExplainedDispersionDatasets():
        covarianceMatrix = getCovarianceMatrixDatasets()
        eigenValueDatasets = getEigenValue(covarianceMatrix)
        return getExplainedDispersion(eigenValueDatasets)
    
    viewGraficsDispersionStart()
    
def learnKNeighborsClassifier(points, reaction):
    def learnKNeighborsClassifierStart():
        neighborsCount=3
        print('Исходные данные')
        learnKNeighborsClassifierByData(points, reaction,neighborsCount)
        print('Исходные нормализованные данные')
        kNeighborsNormalizePoints = learnKNeighborsClassifierByData(getNormalize(points), reaction, neighborsCount)
        scores = tenFoldValidation(kNeighborsNormalizePoints, points, reaction)
        print("Результат кросс-валидации:", scores)
        accuracy = getAccuracyTenFoldValidation(kNeighborsNormalizePoints, points, reaction)
        print('Оценка кросс-валидации:', accuracy)
        print("Дисперсия точности: ", np.var(scores))
        
        accuracyKNeighbors = getAccuracyKNeighborsClassifierForRangeParameters(getNormalize(points), reaction)
        #print("Таблица точности при разных значениях числа соседий и кросс валидации: ", accuracyKNeighbors)
        #viewAccuracyKNeighbord(accuracyKNeighbors)
        viewMaxElemAccuracyKNeighbord(accuracyKNeighbors)
        data = getAccuracyKNeighbord(accuracyKNeighbors)
        matrih = np.matrix(data)
        createGrafics( matrih)
        
    def createGrafics(date):
         def createLabels(figure3D):
            figure3D.set_title("First three PCA directions")
            figure3D.set_xlabel("Число соседей ")
            figure3D.w_xaxis.set_ticklabels(range(1, 11))
            figure3D.set_ylabel("Кол-во кросс валидации")
            figure3D.w_yaxis.set_ticklabels(range(2, 11))
            figure3D.set_zlabel("Значение точности")
            figure3D.w_zaxis.set_ticklabels([])
            return figure3D
        
         baseFigure = plt.figure(1, figsize=(8, 6))
         figure3D = Axes3D(baseFigure, elev=-150, azim=110)
         figure3D.scatter(xs=np.array(date[:, 0].T), 
                          ys=np.array(date[:, 1].T),
                          zs=np.array(date[:, 2].T), edgecolor='k', s=80)
         figure3D = createLabels(figure3D)   
         plt.show()
    
    def getAccuracyKNeighbord(accuracyKNeighbors):
        data =[]
        for accuracyKNeighbord in accuracyKNeighbors:
            for i, d in enumerate(accuracyKNeighbord):
                #print(list(d.values()))
                #print(d.values())
                data.append(list(d.values()))
        return data
                
    def viewAccuracyKNeighbord(accuracyKNeighbors):
        for accuracyKNeighbord in accuracyKNeighbors:
            for i in accuracyKNeighbord:
                print(i)

    
    def learnKNeighborsClassifierByData(points, reaction, neighborsCount):
        pointsTrain, pointsTest, reactionTrain, reactionTest = train_test_split(points, reaction, test_size=0.1, random_state=12)
        
        neigh = KNeighborsClassifier(n_neighbors=neighborsCount)
        
        neigh.fit(pointsTrain, reactionTrain)
        
        y_PredictClassLabels = neigh.predict(pointsTest)
        
        target_names = ['класс 0', 'класс 1']
        print(classification_report(reactionTest, y_PredictClassLabels, target_names=target_names))
        print('Точность классификации:',accuracy_score(reactionTest, y_PredictClassLabels))
        return neigh
    
    def tenFoldValidation(kNeighbors, points, reaction):
        #Сделать 10-fold кросс-валидацию при фиксированном k, оценить дисперсию.
        from sklearn.model_selection import cross_val_score
        scores = cross_val_score(kNeighbors, points, reaction, cv=10)
        return scores
    
    def getAccuracyTenFoldValidation(kNeighbors, points, reaction):
        pred = cross_val_predict(kNeighbors, points, reaction, cv=10)
        return accuracy_score(pred, reaction)

        
        
    learnKNeighborsClassifierStart()

def viewMaxElemAccuracyKNeighbord(accuracyKNeighbors):
    maxElem = findMaxAccuracyValue(accuracyKNeighbors)
    print("Максимальное значение точности достигается для : ", maxElem)
         
def findMaxAccuracyValue(accuracyKNeighbors):
    maxElem = accuracyKNeighbors[0][0]
    for accuracyKNeighbord in accuracyKNeighbors:
        for elem in accuracyKNeighbord:
            difference = float(maxElem['Значение точности']) - float(elem['Значение точности'])
            if difference < 0:
                maxElem = elem

    return maxElem      

def getAccuracyKNeighborsClassifierForRangeParameters(points, reaction):
    def getAccuracyKNeighborsClassifierForRangeParametersStart():
        result = []
        for neighborsCount in range(1, 10):
            reports = getAccuracyKNeighborsClassifier(neighborsCount)
            result.append(reports)
        return result
    
    def getAccuracyKNeighborsClassifier(countNeighbors):
        def getAccuracyKNeighborsClassifierStart():
            reports = []
            neigh = KNeighborsClassifier(n_neighbors=countNeighbors)
            for countKFold in [2, 5, 8, 10]:
                report = getReportAccuracyScoreForNeigh(neigh, countKFold, countNeighbors)
                reports.append(report)
            return reports
            
        def getReportAccuracyScoreForNeigh(neigh, countKFold, countNeighbors):
            def getDispersion(countKFold):
                scores = cross_val_score(neigh, points, reaction, cv=countKFold)
                return np.var(scores)
                
                
            predicted_values = cross_val_predict(neigh, points, reaction, cv=countKFold)
            
            #scores = cross_val_score(neigh, points, reaction, cv=countKFold)
            accuracyScoreForNeighCount = accuracy_score(predicted_values, reaction)
            report = {
                'Число соседей' : countNeighbors,
                'Кол-во кросс валидации' : countKFold,
                'Значение точности': accuracyScoreForNeighCount,
                'Дисперсия': getDispersion(countKFold)
            }
            return report
        
        return getAccuracyKNeighborsClassifierStart()
    
    return getAccuracyKNeighborsClassifierForRangeParametersStart()    

def learnKNeighborsClassifierByDatasetsBreast():
    datasetsBreast = getDatasetsBreast()
    points = datasetsBreast.data
    reaction = datasetsBreast.target
    learnKNeighborsClassifier(points, reaction)

def learnKNeighborsClassifierByPCADatasetsBreast():
    def getReducedPCADatasets(points, counctMainComponents):
        pca = PCA(n_components=counctMainComponents)
        reducedDataset = pca.fit_transform(points)
        return reducedDataset

    datasetsBreast = getDatasetsBreast()
    points = getNormalize(datasetsBreast.data)
    reaction = datasetsBreast.target
    for counctMainComponents in range(1, 10): 
        print('Количество главных компонент', counctMainComponents)
        reducedDataset = getReducedPCADatasets(points, counctMainComponents)
        accuracyKNeighbors = getAccuracyKNeighborsClassifierForRangeParameters(reducedDataset, reaction)
        #print("Таблица точности при разных значениях числа соседий и кросс валидации: ", accuracyKNeighbors)
        #viewAccuracyKNeighbord(accuracyKNeighbors)
        viewMaxElemAccuracyKNeighbord(accuracyKNeighbors)
   
main()

